package com.androj.business.boundary;

import com.androj.business.control.ExceptionControl;
import com.androj.business.exception.BusinessCheckedException;

import javax.inject.Inject;

/**
 * Created by andrzejhankus on 19/09/16.
 */
public class ExceptionBoundary {

    @Inject
    ExceptionControl control;

    public void getCheckedException() throws BusinessCheckedException {
        control.getBusinessException();
    }

    public void getUncheckedException(){
        control.getRuntimeException();
    }

}
