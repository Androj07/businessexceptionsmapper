package com.androj.business.control;

import com.androj.business.exception.BusinessCheckedException;
import com.androj.business.exception.SubBusinessException;

/**
 * Created by andrzejhankus on 19/09/16.
 */
public class ExceptionControl {

    public void getBusinessException() throws BusinessCheckedException {
        throw new SubBusinessException();
    }

    public void getRuntimeException(){
        throw new RuntimeException();
    }
}
