package com.androj.resource;

import com.androj.business.boundary.ExceptionBoundary;
import com.androj.business.exception.BusinessCheckedException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by andrzejhankus on 19/09/16.
 */
@Path("/business")
@Stateless
public class BusinessExceptionResource {

    @Inject
    ExceptionBoundary boundary;

    @GET
    @Path("/checked")
    public Response getOkOrCheckedException() throws BusinessCheckedException {
        boundary.getCheckedException();
        return Response.ok().build();
    }

    @GET
    @Path("/unchecked")
    public Response getOkOrUncheckedException() throws BusinessCheckedException {
        boundary.getUncheckedException();
        return Response.ok().build();
    }
}
