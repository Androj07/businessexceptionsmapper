package com.androj.resource;

import com.androj.business.boundary.ExceptionLocalizationService;
import com.androj.business.exception.BusinessCheckedException;
import com.androj.business.exception.SubBusinessException;
import com.sun.org.apache.bcel.internal.generic.RETURN;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by andrzejhankus on 19/09/16.
 */
@Provider
public class BusinessExceptionMapper implements ExceptionMapper<BusinessCheckedException> {
    @Inject
    ExceptionLocalizationService localizationService;

    @Override
    public Response toResponse(BusinessCheckedException exception) {
        if(exception instanceof SubBusinessException){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .header("cause",localizationService.getLocalizedExceptionMessage())
                    .build();
        }
        return Response.serverError().build();
    }
}
